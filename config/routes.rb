Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "articles#index"
  get "/articles", to: "articles#index"
  # get "/articles/new", to: "articles#new"
  # get "/articles/:id", to: "articles#show"
  get "/articles/tinh", to: "articles#tinh"
  resources :articles do
    resources :comments
  end
end
