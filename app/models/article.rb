class Article < ApplicationRecord
  # include Visible
  has_many :comments
  #một article có thể có nhiều comments

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }
end
