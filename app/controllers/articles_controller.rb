class ArticlesController < ApplicationController
  def index
    #nạp cơ sở dữ liệu vào controller
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end
  def tinh
  end

  def create
    # @article = Article.new(params.require(:article).permit(:title, :body))
    @article = Article.new(article_params)
    if @article.save
      redirect_to @article
    else
      render :new
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end
  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to root_path
  end

  private
    def article_params
      puts ("============== #{params} sadaaa+++++++++++++++++++++++++++++++")
      params.require(:article).permit(:title, :body)
    end
end
